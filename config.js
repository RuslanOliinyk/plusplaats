// path for folder/files
var devPaths = {
  allCss: 'src/scss/bower.scss',
  scss: 'src/scss/',
  css: 'src/css/',
  scripts: 'src/js/',
  images: 'src/img/',
  fonts: 'src/fonts/',
  html: 'src/',
  headerFolder: 'src/',
  headerTpl: 'src/*.html'
}
var distPaths = {
  root: 'dist/',
  css: 'dist/css/',
  scripts: 'dist/js/',
  images: 'dist/img/',
  fonts: 'dist/fonts/',
  html: 'dist/',
  headerFolder: 'dist/'
}


// browserSync
var sync = {
  serverboolean: false,
  server: {
    files: ['{template-parts}/**/*.php', '*.php'],
    proxy: 'http://',
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  },
  noserver: {
    server: {
      baseDir: "src/",
      routes: {"/bower_components": "bower_components"}
    }
  }
}


// change paths, exclude files from bower
var bowerFiles = {
  fileTypes: {
    html: {
      replace: {
        // js: '<script src="/wp-content/themes/CF_blog/{{filePath}}"></script>',
        // css: '<link rel="stylesheet" href="/wp-content/themes/CF_blog/{{filePath}}" />'
      }
    }
  },
  exclude: [
    // 'bower_components/tether/dist/js/tether.js',
    // 'bower_components/bootstrap/dist/js/bootstrap.js'
  ]
}


// custom bootstrap
var bowerBootstrapSrc = 'bower_components/bootstrap/js/dist/'
var bowerBootstrapDist = 'bower_components/bootstrap/dist/js/'
var bootstrapComponents = [
  bowerBootstrapSrc + 'util.js',
  bowerBootstrapSrc + 'alert.js',
  // bowerBootstrapSrc + 'button.js',
  // bowerBootstrapSrc + 'carousel.js',
  bowerBootstrapSrc + 'collapse.js',
  bowerBootstrapSrc + 'dropdown.js',
  bowerBootstrapSrc + 'modal.js',
  // bowerBootstrapSrc + 'popover.js',
  // bowerBootstrapSrc + 'scrollspy.js',
  // bowerBootstrapSrc + 'tab.js',
  // bowerBootstrapSrc + 'tooltip.js'
]


// combine files
var useref = {//on real projects with backend may be issues
  transformPath: function(filePath) {//this is example how to fix paths when files combining
    return filePath.replace('/wp-content/','../../')
  }
}


// remove css
var purifyCssSrc = [
  devPaths.html + '**/*.html',
  distPaths.scripts + '**/*.js'
]


// critical css
var criticalSrcPages = [
    {
      url: 'index.html',
      css: 'index.html'
    },{
      url: 'search.html',
      css: 'search.html'
    },{
      url: 'detail.html',
      css: 'detail.html'
    }
]
var critical = {
  base: 'dist/',
  // inline: false,
  inline: true,
  ignore: ['@font-face',/url\(/, /.modal/, /*/.dropdown/*/],
  css: [
    'src/css/bower.css',
    'src/css/style.css'
  ],
  minify: true,
  width: 1300,
  height: 750
}


// autoprefixer
var settingsAutoprefixer = {
  browsers: [
    'last 2 versions',
    'safari >= 3.1',
    'android 4'
  ]
}


// service links
var serviceLinks = {
  url: devPaths.headerTpl,
  links: 
    '\n' +
    '<!-- build:css css/all.css -->\n' +
    '<link rel="stylesheet" href="css/bower.css">\n' +
    '<!-- bower:css -->\n' +
    '<!-- endbower -->\n' +
    '<link rel="stylesheet" href="css/style.css">\n' +
    '<!-- endbuild -->\n' +
    '\n' +
    '<!-- build:js js/all.js defer -->\n' +
    '<!-- bower:js -->\n' +
    '<!-- endbower -->\n' +
    '<script src="js/script.js"></script>\n' +
    '<!-- endbuild -->\n' +
    '</head>'
}


module.exports = {
    criticalSrcPages, criticalSrcPages,
    critical: critical,
    bootstrapComponents: bootstrapComponents,
    bowerBootstrapDist: bowerBootstrapDist,
    bowerFiles: bowerFiles,
    devPaths: devPaths,
    distPaths: distPaths,
    purifyCssSrc: purifyCssSrc,
    serviceLinks: serviceLinks,
    settingsAutoprefixer: settingsAutoprefixer,
    sync: sync
}