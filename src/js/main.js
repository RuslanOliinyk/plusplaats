$(document).ready(function() {

	//mobile menu
    $(document).on("click", ".showMenu", function(){
        $(".showMenu").toggleClass("opened");
        $(".toggleMenu").toggleClass("menu-open");
    });

	// scrollDown
	$('.scrollDown').click(function(){
	$("html, body").animate({ scrollTop: 950}, 800);
	return false;
	});
	
	// sticky menu
      // var topOfObjToStick = $(".head-menu").height();
      // $(window).scroll(function () { 
      // 	var windowScroll = $(window).scrollTop();
      //   if (windowScroll > topOfObjToStick) { 
      //       $(".head-menu").addClass("stickyHeader"); 
      //   } else { 
      //       $(".head-menu").removeClass("stickyHeader"); 
      //   }
    // });
      var topOfObjToStick = $(".head-menu").height();
      $(window).scroll(function () { 
      	var windowScroll = $(window).scrollTop();
        if (windowScroll > topOfObjToStick) { 
            $(".head-menu").addClass("stickyHeader");
            $(".head-menu").removeClass("stickyHeader1");
            $(".head-menu").removeClass("stickyHeader2");
        } else if (windowScroll > topOfObjToStick/2) { 
            $(".head-menu").addClass("stickyHeader1");
            $(".head-menu").removeClass("stickyHeader");
            $(".head-menu").removeClass("stickyHeader2");
        } else if (windowScroll > topOfObjToStick/3) { 
            $(".head-menu").addClass("stickyHeader2");
            $(".head-menu").removeClass("stickyHeader");
            $(".head-menu").removeClass("stickyHeader1");
        } else {
        		$(".head-menu").removeClass("stickyHeader");
        		$(".head-menu").removeClass("stickyHeader1");
            $(".head-menu").removeClass("stickyHeader2");
        }
    });

  // Slider home page
      var mySwiper = new Swiper('.mob_slide', {
        slidesPerView: 4,
        spaceBetween: 65,
        autoplay: false,
        touchMoveStopPropagation: true,
        nextButton: '.swiper-button-prev',
        prevButton: '.swiper-button-next',
        breakpoints: {
          1200: {
            slidesPerView: 3,
            autoplay: 4000,
            spaceBetween: 15
          },
          991: {
            slidesPerView: 2,
            autoplay: 4000,
            spaceBetween: 10
          },
          767: {
            slidesPerView: 1.3,
            autoplay: 4000,
            spaceBetween: 5,
            centeredSlides: true
          }
        }
    });

  // slider Details page
  function filterViewSlide() {
    var mySwiper = new Swiper('.mob_slide_prod', {
        slidesPerView: 4,
        spaceBetween: 65,
        autoplay: false,
        touchMoveStopPropagation: true,
        nextButton: '.swiper-button-prev',
        prevButton: '.swiper-button-next',
    		breakpoints: {
    			1200: {
            slidesPerView: 3,
            autoplay: 4000,
            spaceBetween: 15
          },
			    991: {
            slidesPerView: 2,
            autoplay: 4000,
            spaceBetween: 10
          },
          767: {
            slidesPerView: 1.3,
            autoplay: 4000,
            spaceBetween: 5,
            centeredSlides: true
          }
        }
    });
  }
  filterViewSlide();
  $(window).resize(function() { 
      filterViewSlide();
  });

// slider
    var mySwiper2 = new Swiper('.detail2', {
        slidesPerView: 4,
        spaceBetween: 65,
        autoplay: false,
        touchMoveStopPropagation: true,
        nextButton: '.swiper-button-prev2',
        prevButton: '.swiper-button-next2',
        breakpoints: {
          1200: {
            slidesPerView: 3,
            autoplay: 4000,
            spaceBetween: 15
          },
          991: {
            slidesPerView: 2,
            autoplay: 4000,
            spaceBetween: 10
          },
          767: {
            slidesPerView: 1.3,
            autoplay: 4000,
            spaceBetween: 5,
            centeredSlides: true
          }
        }
    });

  // Search page slider
    var mySwiper3 = new Swiper('.search_mob_slide', {
        slidesPerView: 3,
        spaceBetween: 30,
        autoplay: false,
        touchMoveStopPropagation: true,
        breakpoints: {
          1200: {
            slidesPerView: 3,
            autoplay: 4000,
            spaceBetween: 15
          },
          991: {
            slidesPerView: 2,
            autoplay: 4000,
            spaceBetween: 10
          },
          767: {
            slidesPerView: 1.3,
            autoplay: 4000,
            spaceBetween: 5,
            centeredSlides: true
          }
        }
    });

  //header btn dropdown
  $(document).on("click", ".panel_heading", function(){
    $(".panel_body").toggleClass("show_sidebar");
  });
  //mobile sidebar search page
  $(document).on("click", ".custom_btn", function(){
    // $(".dropdown_body").toggleClass("show_dropdown");
    $(this).find('.dropdown_body').toggleClass("show_dropdown");
  });
  // rotate arrows
  $(document).on("click", ".panel_heading", function(){
    $(this).find('.sidebar_arrow').toggleClass("rotate");
  });
  $(document).on("click", ".btn-secondary", function(){
    $(this).find('.sidebar_arrow').toggleClass("rotate");
  });
  // $(document).on("click", ".sorteer_btn", function(){
  //   $(this).find('.sidebar_arrow').toggleClass("rotate");
  // });

  // tab-slider
  slider_for_mobile();
  $(window).resize(function() {
    slider_for_mobile();
  });
  function slider_for_mobile(){
    swiper1 = undefined
    if (window.matchMedia('(max-width: 991px)').matches) {
      $(".tabs .tab-content").addClass("swiper-wrapper")
      $(".tabs .tab-content").removeClass("tab-content")
      $(".tabs .swiper-wrapper .swiper-slide").removeClass("tab-pane")
      $(".tabs .swiper-wrapper .swiper-slide").removeClass("fade")
      $(".tabs .swiper-wrapper .swiper-slide").removeClass("show")
        var swiper1 = new Swiper('.swiper-mobile',{
        slidesPerView: 1,
        centeredSlides: true,
        spaceBetween: 60,
        paginationClickable: true,
        pagination:'.swiper-pagination'
      });
    } else{
      swiper1 = undefined;
      $(".tabs .swiper-wrapper").addClass("tab-content")
      $(".tabs .swiper-wrapper").removeClass("swiper-wrapper")
      $(".tabs .tab-content .swiper-slide").addClass("tab-pane")
      $(".tabs .tab-content .swiper-slide").addClass("fade")
      $(".tabs .tab-content .swiper-slide").addClass("show");
      console.log("ERer")
    };
  };

});

