<h5>Ready workspace for frontend for many tasks.</h5>
* js: clean unnecessary js from bootstrap.js, concatenation all js files into one minified files
* css: clean unnecessary css (actually good works only for static pages), compilation scss to css, concatenation all css files into one minified files, extracts & inlines critical (above-the-fold) CSS to html
* images: image optimization, convertation to webp
<br>



<h1>Quick start</h1>
1. `npm i` - install npm packages
2. `bower i` - install bower dependencies
3. `gulp`<sup>*</sup> - run default task (development) or `gulp build` - production version of code.
<br> * `gulp firsttask` - before gulp needs to run this task for to insert css/js resources when creates project (below clarification)


<h1>Bower</h1>
bower needs for to forgot about downloading components (js plugins, frameworks like bootstrap, fontawesome etc) from different sites, unzipping and including to project - all this makes bower/gulp. Bower downloading components and other staff - gulp.

All dependencies (like bootstrap, fontawesome etc) listed in **bower.json** file. So, for add/remove any dependencies needs just to change this file and after run `bower i` - will be download all components, default task `gulp` - dependencies will be auto-include to project.<br>
Bower will include css/js files between related comments in the header:<br>
`<!-- bower:css -->`<br>
`<!-- endbower -->`<br>
`<!-- bower:js -->`<br>
`<!-- endbower -->`<br>

scss files will include to the bower.scss


<h1>Gulp functions.</h1>
For almost all projects (static and dynamic sites) needs to change file **config.js** - in this file paths to folders/files and some other settings, and file **bower.json** - because each project has different set of components.
There are three main functions in **gulpfile.js**:
1. gulp firsttask - this task created just to paste this required code to header:<br>
    `<!-- build:css css/all.css -->`<br>
    `<link rel="stylesheet" href="css/bower.css">`<br>
    `<!-- bower:css -->`<br>
    `<!-- endbower -->`<br>
    `<link rel="stylesheet" href="css/style.css">`<br>
    `<!-- endbuild -->`<br>
    
    `<!-- build:js js/all.js defer -->`<br>
    `<!-- bower:js -->`<br>
    `<!-- endbower -->`<br>
    `<script src="js/script.js"></script>`<br>
    `<!-- endbuild -->`<br>


2.  gulp - this task contains 3 different tasks: 
    a) `bower` - includes components to the page (css, js, fontawesome copy-pasting)<br>
    b) `sass` - compilation scss to css with maps + applying autoprefixer for crossbrowsering<br>
    c) `browserSync` - page reload<br>
    d) `watch` - watching on changed template/js/scss.<br>
    Tasks c) and d) allows to get live reload if were changed template/js and injecting css if was changed scss(it means that you changed scss - it compiles to css on fly and injects to page) 
3.  gulp build - set of tasks for production, production files placed by default into dist folder:<br>
    a) `clean:dist` - remove all production tasks<br>
    b) `sass` - the same as in previous point 2) - but css without map<br>
    c) `['useref', 'images', 'fonts']` - useref - concatenation css/js files (which between related comments <!-- build:css css/all.css --> and <!-- build:js js/all.js defer -->); images - image optimization, fonts - copy-pasting fonts to dist.<br>
    d) `minify` - css minification.<br>
    e) `critical` - critical css for each page.<br>
